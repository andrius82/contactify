import React from 'react';
import '../scss/NavMain.scss';


function NavMain() {
    return(
        <nav className="main-nav">
            <ul>
                <li className="first">
                    <a href="/">Dashboard</a>
                </li>
                <li>
                    <a href="/">Contacts</a>
                </li>
                <li className="last">
                    <a href="/">Notifications</a>
                </li>
            </ul>
        </nav>
    );
}


export default NavMain;