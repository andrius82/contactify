import React, { useState, useEffect } from 'react';
import '../scss/App.scss';
import contactsjson from "../contacts.json";

import NavMain from './NavMain';
import SearchMain from './SearchMain';
import UserBar from './UserBar';
import ContactsList from './ContactsList';

import logo from '../mainlogo.jpg';


function App() {
  const [data, setData] = useState(null);

	useEffect(() => {
    setData(contactsjson);
	}, []);


  return (
    <>
    <div className="main-wrapper">
      <header className="header" >
        <div className="section section--1188">
            <a className="mainlogo"  href="/"><img src={logo} title="Contactify" alt="Contactify" /></a>
            <NavMain/>
            <SearchMain/>
            <UserBar/>
        </div>
      </header>
      <main className="main">
        {data && <ContactsList contacts={data} />}
      </main>
    </div>
      <footer className="footer">
        <div className="section section--1188 footer__section">
          <div className="footer__block footer__block--1">
            <a className="footer__item" href="/">Dashboard</a>
            <a className="footer__item" href="/">Contacts</a>
            <a className="footer__item" href="/">Notifications</a>
            <div className="footer__items2">
              <a className="footer__copyr" href="/">&copy; 2015 Contactify</a>
              <a className="footer__item2" href="/">About</a>
              <a className="footer__item2" href="/">Privacy</a>
            </div>
          </div>
          <div className="footer__block footer__block--2">
            <div className="blck-smlico">
              <div className="blck-smlico__col first">
                <i className="fas fa-cloud-upload"></i>
              </div>
              <div className="blck-smlico__col">
                <div className="blck-smlico__colleft">
                  <div className="blck-smlico__item">Last synced:</div> 
                  <div className="blck-smlico__item">2015-06-02 14:33:10</div>
                </div>
                <a href="/" className="blck-smlico__item2">
                  <svg aria-hidden="true" 
                        focusable="false" 
                        data-prefix="fas" 
                        data-icon="sync-alt" 
                        role="img" 
                        xmlns="http://www.w3.org/2000/svg" 
                        viewBox="0 0 512 512" 
                        className="svg-inline--fa fa-sync fa-w-16 fa-9x">
                        <path fill="currentColor" d="M370.72 133.28C339.458 104.008 298.888 87.962 255.848 88c-77.458.068-144.328 53.178-162.791 126.85-1.344 5.363-6.122 9.15-11.651 9.15H24.103c-7.498 0-13.194-6.807-11.807-14.176C33.933 94.924 134.813 8 256 8c66.448 0 126.791 26.136 171.315 68.685L463.03 40.97C478.149 25.851 504 36.559 504 57.941V192c0 13.255-10.745 24-24 24H345.941c-21.382 0-32.09-25.851-16.971-40.971l41.75-41.749zM32 296h134.059c21.382 0 32.09 25.851 16.971 40.971l-41.75 41.75c31.262 29.273 71.835 45.319 114.876 45.28 77.418-.07 144.315-53.144 162.787-126.849 1.344-5.363 6.122-9.15 11.651-9.15h57.304c7.498 0 13.194 6.807 11.807 14.176C478.067 417.076 377.187 504 256 504c-66.448 0-126.791-26.136-171.315-68.685L48.97 471.03C33.851 486.149 8 475.441 8 454.059V320c0-13.255 10.745-24 24-24z" className=""></path>
                  </svg>
                  Force sync
                </a>
              </div>
            </div>
            <div className="blck-smlico blck-smlico--2">
              <div className="blck-smlico__col first">
                <i className="fas fa-stethoscope"></i>
              </div>
              <div className="blck-smlico__col">
                <div className="blck-smlico__item">Help desk and Resolution center available:</div>
                <div className="blck-smlico__item">I-IV 8:00-18:00, V 8:00-16:45</div>
              </div>
            </div>
          </div>
          <div className="footer__block footer__block--3">
            <a className="footer__item" href="/">Groups</a>
            <a className="footer__item" href="/">Frequently contacted</a>
            <a className="footer__item" href="/">Preference</a>
            <a className="footer__item" href="/">Log out</a>
          </div>
        </div>
      </footer>
    </>
  );
}

export default App;
