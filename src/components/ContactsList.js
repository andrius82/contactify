import React, { useState } from 'react';
import '../scss/ContactsList.scss';

import ContactDetails from './ContactDetails';
import ContactsFilter from './ContactsFilter';

import _ from 'lodash';


function ContactsList(props) {
	const [list, setList] = useState(props.contacts);
	const [selected, setSelected] = useState(props.contacts[0]);
	const [orderlist, setOrderlist] = useState(null);


    function orderByName() {
        if (orderlist === null) {
            const sortedlist = _.orderBy(list, ['name'], ['asc']);
            setList(sortedlist);
            setOrderlist(true);
        }else{
            const sortedlist = _.orderBy(list, ['name'], [!orderlist ? 'asc' : 'desc']);
            setList(sortedlist);
            setOrderlist(!orderlist);
        }
    }


    function selectContact(contact) {
        setSelected(contact);
    }


    function deleteContact(id) {
        const newlist = [...list];

        newlist.forEach(contact => {
            if (contact.id === id) {
                contact.delete = true;
            }
        });
        
        // after deleting keep open next first contact
        if (newlist.length > 1) {
            const viewcontact = list.filter(item => item.delete !== true)[0];
            setSelected(viewcontact); 
        }else {
            setSelected({});
        }

        setList(newlist);
    }

    function handleFilter(params) {
        let newlist = [];

        if (orderlist !== null) {
            newlist = _.orderBy(props.contacts, ['name'], [orderlist ? 'asc' : 'desc']);
        }else {
            newlist = [...props.contacts]; 
        }

        if (_.has(params, 'name') && params.name !== "") {
            const searchString = params.name.toLowerCase();
            var regex = new RegExp("^" + searchString, "g");
            newlist = newlist.filter(item => item.name.toLowerCase().search(regex) !== -1);
        }

        if (_.has(params, 'active') && params.active === true) {
            newlist = newlist.filter(item => item.active === true);
        }

        if (_.has(params, 'city') && params.city !== "0") { 
            newlist = newlist.filter(item => item.city === params.city);
        }

        // after filtering keep open first contact
        if (list.length >= 1) {
            const viewcontact = newlist.filter(item => item.delete !== true)[0];
            setSelected(viewcontact); 
        }else {
            setSelected({});
        }

        setList(newlist);
    }


    return (
        <div className="clist">
            <div className="grad-greenblue-darker clist__filter">
                <div className="section section--1188">
                    <ContactsFilter list={props.contacts} filterParams={(params) => handleFilter(params)} />
                </div>
            </div>
            <div className="section section--1188 clist__section">
                <div className="clist__sidebar">
                    <ContactDetails {...selected} />
                </div>
                <div className="container container--white container--rounded clist__main">
                    <div className="clist__thead">
                        <div className={`clist__lbl clist__lbl--blue c-1 name ${orderlist !== null ? (orderlist ? 'asc' : 'dsc') : ""}`} onClick={() => orderByName()} >Name<i className="fas fa-arrow-down"></i></div>
                        <div className="clist__lbl clist__lbl--blue c-2">Surname</div>
                        <div className="clist__lbl clist__lbl--blue c-3">City</div>
                        <div className="clist__lbl clist__lbl--blue c-4">Email</div>
                        <div className="clist__lbl clist__lbl--blue c-5 txt-right">Phone</div>
                        <div className="clist__lbl clist__lbl--blue c-6"></div>
                    </div>
                    <div className="clist__tbody">   
                        {list.filter(item => item.delete !== true).map(contact => (
                            <div className={`clist__itemrow ${selected.id === contact.id ? 'active' : ""}`} key={contact.id}>
                                <div className="clist__cell c-1" onClick={() => selectContact(contact)} >
                                    {contact.active ? <i className="far fa-eye"></i> : <i className="far fa-eye-slash"></i>}
                                    {contact.name}
                                </div>
                                <div className="clist__cell c-2" onClick={() => selectContact(contact)}>{contact.surname}</div>
                                <div className="clist__cell c-3" onClick={() => selectContact(contact)}>{contact.city}</div>
                                <div className="clist__cell c-4" onClick={() => selectContact(contact)}>{contact.email}</div>
                                <div className="clist__cell c-5" onClick={() => selectContact(contact)}>{contact.phone}</div>
                                <div className="clist__cell c-6">
                                    <span className="edit btn" ><i className="fas fa-pencil"></i></span>
                                    <span className="delete btn" onClick={() => deleteContact(contact.id)} ><i className="fas fa-trash"></i></span>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}


export default ContactsList;