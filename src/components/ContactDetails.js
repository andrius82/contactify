import React from 'react';
import '../scss/ContactDetails.scss';

import userpic from '../images/userpic.jpg';

const ContactDetails = (props) => (
    <>
        {props.name && 
            <div className="block-pic block-pic--white block-pic--rounded">
                <div className="block-pic__sec clearfix">
                    <div className="block-pic__imgwrp">
                        <img src={userpic} className="block-pic__img block-pic__img--grey" title="" alt=""/>
                    </div>
                    <div className="block-pic__items">
                        <div className="block-pic__item"><span>Name</span><div>{props.name}</div></div>
                        <div className="block-pic__item"><span>Surname</span><div>{props.surname}</div></div>
                        <div className="block-pic__item"><span>City</span><div>{props.city}</div></div>
                        <div className="block-pic__item"><span>Email</span><div><a href={`tel:${props.email}`}>{props.email}</a></div></div>
                        <div className="block-pic__item"><span>Phone</span><div>{props.phone}</div></div>
                    </div>
                </div>
            </div>
        }
    </>
);


export default ContactDetails;