import React, { useState } from 'react';
import '../scss/ContactsFilter.scss';


function ContactsFilter(props) { 

    const [searchValue, setSearchValue] = useState("");
    const [selectedCity, setSelectedCity] = useState("0");
    const [isActive, setIsActive] = useState(false);

    const cities = [...new Set(props.list.map(item => item.city))];
    cities.sort();


    function handleInputChange(event) {
        if (event.target.name === 'sortbyname') {
            props.filterParams({name: event.target.value, city: selectedCity, active: isActive});
            setSearchValue(event.target.value);
        }
        if (event.target.name === 'cities') {
            props.filterParams({name: searchValue, city: event.target.value, active: isActive});
            setSelectedCity(event.target.value);
        }
        if (event.target.name === 'isactive') {
            props.filterParams({name: searchValue, city: selectedCity, active: !isActive});
            setIsActive(!isActive);	
        }
    }

    return (
        <div className="filter">
            <form onSubmit={(ev) => ev.preventDefault()} >
                <div className="form-item">
                    <input 
                        id="sortbyname" 
                        name="sortbyname"
                        type="text" 
                        placeholder="Name" 
                        className="inputrnd inputrnd--grey"
                        onChange={(ev) => handleInputChange(ev)}  
                    />
                </div> 
                <div className="form-item cities-item">
                    <select name="cities" id="cities" className="select select--grey" onChange={(ev) => handleInputChange(ev)} >
                        <option value="0">City</option>
                        {cities.map(city => <option key={city} value={city}>{city}</option>)}
                    </select>
                </div> 
                <div className={`form-item checkbox-item ${isActive ? "checked" : ""}`} >
                    <input 
                        id="isactive" 
                        name="isactive"
                        type="checkbox" 
                        checked={isActive} 
                        className="checkbox checkbox--grey"
                        onChange={(ev) => handleInputChange(ev)}
                    />
                    <label htmlFor="isactive">Show active</label>
                </div> 
            </form>
            <div className="btn-rnd btn-rnd--green add-contract"><i className="fas fa-plus"></i><span>Add New Contract</span>
            </div>
        </div>
    );

}


export default ContactsFilter;