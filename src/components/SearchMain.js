import React from 'react';
import '../scss/SearchMain.scss';


function SearchMain() {
    return(
        <div className="main-search">
            <form onSubmit={(ev) => ev.preventDefault()} >
                <input 
                    id="search" 
                    name="search" 
                    type="text" 
                    placeholder="Search" 
                    className="" 
                />
                <button className="main-search__btn" ><i className="fas fa-search"></i></button>
            </form>
        </div>
    );
}


export default SearchMain;