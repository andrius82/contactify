import React, { useState } from 'react';
import '../scss/UserBar.scss';


function UserBar() {

	const [isVisible, setIsVisible] = useState(false);

    return(
        <div className="user-bar">
	        <div className="user-bar__title" onClick={() => setIsVisible(!isVisible)} >
	        	<i className="fas fa-user"></i>Jorah Mormont<i className={`fas fa-sort-down ${isVisible ? "up" : ""}`}></i>
	        </div>
	        <div className={`user-bar__tooltip ${isVisible ? "visible" : ""}`}>
	        	<div className="clip"></div>
				<div className="user-bar__items">	
					<a className="user-bar__item" href="/">
						<i className="fas fa-users"></i>
						Groups
					</a>
					<a className="user-bar__item" href="/">
						<i className="fas fa-comments"></i>
						Frequently contacted
					</a>
					<a className="user-bar__item" href="/">
						<i className="fas fa-wrench"></i>
						Preference
					</a>
					<a className="user-bar__item" href="/">
						<i className="fas fa-sign-out"></i>
						Log out
					</a>
				</div>
        	</div>
        </div>
    );
}


export default UserBar;